# Royal Game of Ur #


This README contains some basic details about the simulation of the Royal Game of Ur and how to run the simulation. This is currently a CPU vs. CPU setup, but could be modified to run a human vs. CPU and even human vs. human for future updates.  

## Requirements ##
This was written with Python 3.7. There are no known issues with another version of Python 3, but they are not tested. 

### Required Python Libraries ###
- pygame
- colorlog
- numpy
 
## Command Options ##

* -V, --version | Display the version number
* -v, --verbose | Impacts the amount of information displayed in the terminal (can send up none to -vvvv)
* -g, --games | Number of games to simulate (default 100)
* -p1, --player1 |  Set player 1 strategy. Set to 100 for fully agreesive, 0 for fulley passive, or a number between 1 and 99 for that level of agreesive and the 1-X is set to passive (default 100)
* -p2, --player2 | See -p1 but impacts player 2.

## Starting a Simulation ##
The following command shall be executed to start a simulation with default settings:
 
    :$ python royalGameOfUr.py   
    
Here is an example running a thousand games, with player 1 fully aggressive and player 2 fully defensive (passive):
 
    :$ python royalGameOfUr.py -g 1000 -p1 100 -p2 0   

