#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - gameDice.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This file contains the class for the game dice and to determine the player 
# that goes first. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import random

# ===[ CLASSES ]================================================================
# ------------------------------------------------------------------------------
# Class: GameDice
#
# Class containing methods to roll the dice for the Game of Ur and other random
# choice events used throughout the game. 
#-------------------------------------------------------------------------------
class GameDice(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self):
        self.logger.debug("Dice are ready to roll!")

    # --------------------------------------------------------------------------
    # Function: roll
    #
    # Rolls four, 4-sided dice. If the random value is 2 or 4, it is considered
    # a painted edge and counts as 1. Otherwise it is a 0. The total is 
    # returned for the value of the roll. 
    #---------------------------------------------------------------------------
    def roll(self):
        count = 0
        for die in range(0,4):
            newValue = random.SystemRandom().randint(1, 4)
            if (newValue % 2):
                count += 1
        self.logger.debug('Dice Roll: {}'.format(count))
        return count

    # --------------------------------------------------------------------------
    # Function: determineStartingPlayer
    #
    # Rolls four, 4-sided dice to determine which player goes first. Even roll
    # player 2 goes first, odd roll player 1.
    #---------------------------------------------------------------------------
    def determineStartingPlayer(self):
        player = 0
        value = self.roll()
        if (value % 2):
            player = 1
        self.logger.debug('Player {} will start first.'.format(player + 1))
        return player

    # --------------------------------------------------------------------------
    # Function: determinePlayerStrategy
    #
    # Random number between 1 and 100. Based on the strategy assigned to the 
    # player, returns a 0 or 1 (aggr./pass.) to use that strategy in determining 
    # a move for a game piece.
    #---------------------------------------------------------------------------
    def determinePlayerStrategy(self):
        playerArgStrat = self.cfg.playerStrategy[self.pTurn]
        if playerArgStrat == 100:
            returnValue = 0 # Aggressive
        elif playerArgStrat == 0:
            returnValue = 1 # Passive
        else:
            value = random.SystemRandom().randint(0,100)
            if value <= playerArgStrat:
                returnValue = 0 # Aggressive 
            else:
                returnValue = 1 # Passive
        return returnValue

# ===[ EOF ]====================================================================
