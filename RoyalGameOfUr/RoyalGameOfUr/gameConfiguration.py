#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - gameBoard.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This file contains the class definition for the configuration of the game 
# simulation. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System 
import logging
import pygame
import numpy as np
from os import system, name
from operator import attrgetter
from datetime import datetime

# Local 
from library import _PLAYER_1_IDX, _PLAYER_2_IDX

# ===[ CLASSES ]================================================================

# ------------------------------------------------------------------------------
# Class: PlayerStats
#
# Class containing stats for a given player. 
#-------------------------------------------------------------------------------
class PlayerStats(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self):
        self.moves = 0
        self.score = 0
        self.rollValCount = [0,0,0,0,0]
        self.stratCount = [0,0]

    # --------------------------------------------------------------------------
    # Function: incrementValueCount
    #
    # The list index is the roll value, and the count is incremented for the 
    # number of times it is rolled. 
    #---------------------------------------------------------------------------
    def incrementValueCount(self, value):
        self.rollValCount[value] += 1

    # --------------------------------------------------------------------------
    # Function: incrementStratCount
    #
    # The list index is the strategy value, and the count is incremented for the 
    # number of times it is played. 
    #---------------------------------------------------------------------------
    def incrementStratCount(self, value):
        self.stratCount[value] += 1

# ------------------------------------------------------------------------------
# Class: GameRecords
#
# Class containing the game number, time, and player stats. 
#-------------------------------------------------------------------------------
class GameRecords(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self):
        self.gameNumber = 0
        self.playerStats = [PlayerStats(), PlayerStats()]
        self.time = 0

# ------------------------------------------------------------------------------
# Class: GameConfiguration
#
# Class containing the configuration and stats of the game. 
#-------------------------------------------------------------------------------
class GameConfiguration(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self, args):
        self.logger = logging.getLogger('')
        #Set up configuration 
        self.NumberOfGames = args.games
        self.currentGame = 1
        self.playerStrategy = [args.player1, args.player2]
        self.verbose = args.verbose
        self.records = [GameRecords() for i in range(self.NumberOfGames)]

        dateTimeObj = datetime.now()
        self.timestamp = dateTimeObj.strftime("%d%b%Y %H.%M.%S.%f")
        self.logFile = "Game Log_{}_{}_P1-{}_P2-{}.csv".format(self.NumberOfGames, self.timestamp, self.playerStrategy[0], self.playerStrategy[1])

    # --------------------------------------------------------------------------
    # Function: getTotalWins
    #
    # Gathers the number of wins for the requested player from the records.
    #---------------------------------------------------------------------------
    def getTotalWins(self, player):
        return sum(1 for record in self.records if (record.score[player] == 7))

    # --------------------------------------------------------------------------
    # Function: getTotalLoses
    #
    # Gathers the number of loses for the requested player from the records.
    #---------------------------------------------------------------------------
    def getTotalLoses(self, player):
        return sum(1 for record in self.records if (record.score[player ^ 1] == 7))

    # --------------------------------------------------------------------------
    # Function: getTimeMin
    #
    # Get the fastest game time from the records. 
    #---------------------------------------------------------------------------
    def getTimeMin(self):
        data = [record.time for record in self.records if record.gameNumber is not 0]
        if len(data) == 0:
            return 0
        else:
            return min(data)

    # --------------------------------------------------------------------------
    # Function: getTimeMax
    #
    # Get the longest game time from the records. 
    #---------------------------------------------------------------------------
    def getTimeMax(self):
        data = [record.time for record in self.records if record.gameNumber is not 0]
        if len(data) == 0:
            return 0
        else:
            return max(data)

    # --------------------------------------------------------------------------
    # Function: getTimeAverage
    #
    # Get the average game time from the records. 
    #---------------------------------------------------------------------------
    def getTimeAverage(self):
        data = [record.time for record in self.records if record.gameNumber is not 0]
        if len(data) == 0:
            return 0
        else:
            return np.mean(data)

    # --------------------------------------------------------------------------
    # Function: getTimeStdDevi
    #
    # Get the standard deviation time from the records. 
    #---------------------------------------------------------------------------   
    def getTimeStdDevi(self):
        data = [record.time for record in self.records if record.gameNumber is not 0]
        if len(data) == 0:
            return 0
        else:
            return np.std(data)

    # --------------------------------------------------------------------------
    # Function: updateTime
    #
    # Increment the timer for the given game in the records list
    #---------------------------------------------------------------------------
    def updateTime(self):
        self.records[(self.currentGame - 1)].time += 1

    # --------------------------------------------------------------------------
    # Function: getTime
    #
    # Gets the time from the current game entry. 
    #---------------------------------------------------------------------------
    def getTime(self):
        return self.records[(self.currentGame - 1)].time

    # --------------------------------------------------------------------------
    # Function: newGame
    #
    # Increments the current game for a new game, which also adjusts the records
    # that are updated. 
    #---------------------------------------------------------------------------
    def newGame(self):
        self.currentGame += 1
        self.logger.info("---- Game {} has started ----".format(self.currentGame))
        
    # --------------------------------------------------------------------------
    # Function: updateGameRecord
    #
    # Updates teh records for eaceh player after a game has completed.  
    #---------------------------------------------------------------------------
    def updateGameRecord(self, Players):
        self.records[(self.currentGame - 1)].gameNumber = self.currentGame
        for i in range (0, 2):
            self.records[(self.currentGame - 1)].playerStats[i].moves = Players[i].moves
            self.records[(self.currentGame - 1)].playerStats[i].score = Players[i].score
            self.records[(self.currentGame - 1)].playerStats[i].rollValCount = Players[i].rollValCount
        self.writeGameRecord(self.records[(self.currentGame - 1)])
         
    # --------------------------------------------------------------------------
    # Function: writeGameRecord
    #
    # Logs the information to a text (csv) file for analysis.
    #---------------------------------------------------------------------------
    def writeGameRecord(self, record):
        fh = open("./{}".format(self.logFile), 'a')
        # Game Number, Time, [P1 Score, P1 Moves, roll values (5 of them)], repeat player 2
        logEntry = "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(record.gameNumber,
                                                                                          record.time,
                                                                                          record.playerStats[_PLAYER_1_IDX].score,
                                                                                          record.playerStats[_PLAYER_1_IDX].moves,
                                                                                          record.playerStats[_PLAYER_1_IDX].rollValCount[0],
                                                                                          record.playerStats[_PLAYER_1_IDX].rollValCount[1],
                                                                                          record.playerStats[_PLAYER_1_IDX].rollValCount[2],
                                                                                          record.playerStats[_PLAYER_1_IDX].rollValCount[3],
                                                                                          record.playerStats[_PLAYER_1_IDX].rollValCount[4],
                                                                                          record.playerStats[_PLAYER_1_IDX].stratCount[0],
                                                                                          record.playerStats[_PLAYER_1_IDX].stratCount[1],
                                                                                          record.playerStats[_PLAYER_2_IDX].score,
                                                                                          record.playerStats[_PLAYER_2_IDX].moves,
                                                                                          record.playerStats[_PLAYER_2_IDX].rollValCount[0],
                                                                                          record.playerStats[_PLAYER_2_IDX].rollValCount[1],
                                                                                          record.playerStats[_PLAYER_2_IDX].rollValCount[2],
                                                                                          record.playerStats[_PLAYER_2_IDX].rollValCount[3],
                                                                                          record.playerStats[_PLAYER_2_IDX].rollValCount[4],
                                                                                          record.playerStats[_PLAYER_2_IDX].stratCount[0],
                                                                                          record.playerStats[_PLAYER_2_IDX].stratCount[1])
        fh.write(logEntry)
        fh.close()

# ===[ EOF ]==================================================================== 
