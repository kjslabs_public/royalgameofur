#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - commandOptions.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This file contains the arguement parser for the project.
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import argparse
from argparse import RawTextHelpFormatter

# ===[ CONSTANTS ]==============================================================
VERSION = '1.0.1'

# ===[ FUNCTIONS ]==============================================================
# --------------------------------------------------------------------------
# Function: processArg()
#
# Processes the command line options that were passed in by the caller
#---------------------------------------------------------------------------
def processArg():
    description = "Royal Game of Ur\n"
    description += '- Version: %s\n' % VERSION
    description += "- Author(s): Kevin Smith (smit2958@msu.edu),\n"

    # set up the argument parser 
    parser = argparse.ArgumentParser(description=description, formatter_class=RawTextHelpFormatter)

    # Add optional arguments
    parser.add_argument(
        '-v', '--verbose',
        default = 0,
        action='count',
        help='Output detailed information to the console'
    )

    parser.add_argument(
        '-V', '--version',
        action='version',
        version='%%(prog)s %s' % (VERSION),
        help='Display version number and exit'
    )

    parser.add_argument(
        '-g', '--games',
        default = 100,
        action = 'store',
        type=int,
        help = 'Number of games to play'
    )

    parser.add_argument(
        '-p1', '--player1',
        action = 'store',
        default = 100,
        type=check_range, 
        help = 'Set player 1 strategy. Set to 100 for fully agreesive, 0 for fulley passive, or a number between 1 and 99 for that level of agreesive and the 1-X is set to passive.'
    )

    parser.add_argument(
        '-p2', '--player2',
        action = 'store',
        default = 100,
        type=check_range,
        help = 'Set player 2 strategy. Set to 100 for fully agreesive, 0 for fulley passive, or a number between 1 and 99 for that level of agreesive and the 1-X is set to passive.'
    )

    args = parser.parse_args()
    return args

# --------------------------------------------------------------------------
# Function: check_range()
#
# Processes Checks the argument for p1 and p2 with a range check of 0 to 
# 100. Raises and exception for any value outside of that range. 
#---------------------------------------------------------------------------
def check_range(arg):
    try:
        value = int(arg)
    except ValueError as err:
       raise argparse.ArgumentTypeError(str(err))

    if value < 0 or value > 100:
        message = "Expected 0 <= value <= 100, received {}".format(value)
        raise argparse.ArgumentTypeError(message)

    return value

# ===[ EOF ]====================================================================
