#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - log.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# Used to create a common logging interface that all other submodules will use.
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import logging
import colorlog

# ===[ FUNCTIONS ]==============================================================
# --------------------------------------------------------------------------
# Function: setupLogger
#
# Setup custom logging interface for any module that needs it.  
#---------------------------------------------------------------------------
def setupLogger(name, level):
    logColors = {
        'DEBUG': 'bold_cyan',
        'INFO': 'bold_green',
        'STATUS': 'white',
        'WARNING': 'bold_yellow',
        'ERROR': 'bold_red',
        'CRITICAL': 'bold_red, bg_white'
    }
    format = colorlog.ColoredFormatter('%(asctime)s [%(log_color)s%(levelname)s%(reset)s] %(message)s', log_colors=logColors, datefmt="%H:%M:%S")
    handler = colorlog.StreamHandler()
    handler.setFormatter(format)
    logger = colorlog.getLogger(name)

    if level == 1:
        level = logging.ERROR
    elif level == 2:
        level = logging.WARNING
    elif level == 3:
        level = logging.INFO
    elif level >= 4:
        level = logging.NOTSET
    else:
        level = logging.CRITICAL

    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

# ===[ EOF ]====================================================================
