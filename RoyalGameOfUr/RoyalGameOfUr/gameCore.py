#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - gameCore.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This file contains the core class of the Royal Game of Ur. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import pygame

# Local inputs
import log
from gamePrompts import GamePrompts 
from gameDice import GameDice
from gameBoard import GameBoard
from gamePlayer import GamePlayers
from gameConfiguration import GameConfiguration
import library as lib
import time

# ===[ DICTIONARIES ]==========================================================
_BOARD_DEBUG_INFO = { 'font': lib._FONT_HACK_REGULAR,
                      'size': 10 }

_END_SIM_TEXT = { 'font': lib._FONT_HACK_REGULAR,
                      'size': 16 }

_GAME_TIMER = 60

# ===[ CLASSES ]================================================================
# ------------------------------------------------------------------------------
# Class: RoyalGameOfUr
#
# Core class for the simulator. Loads all the sub classes that are used 
# as an extension.  
#-------------------------------------------------------------------------------
class RoyalGameOfUr(GameBoard, GameDice, GamePlayers):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self, args):
        # Command arguments and set up logger
        self.logger = log.setupLogger('', args.verbose)
        
        # Create an instance of the Game prompt class
        self.Prompts = GamePrompts()

        # Great the user and show them information before staring the game.
        self.Prompts.displayWelcomeMessage()
        self.logger.debug('Command Arguments: ' + str(args))
        self.logger.debug('Created instance of Royal Game Of Ur')

        # Setup of the clock and timer for screen updates
        self.FPS = 60
        self.Clock = pygame.time.Clock()
        self.timerAlarm = pygame.USEREVENT + 1
        self.gameLogicAlarm = pygame.USEREVENT + 2
        pygame.time.set_timer(self.timerAlarm, 1000) # 1000 ms, or 1 sec.
        
        
        # Bring in other classes to load directly in to this class. 
        GameDice.__init__(self)
        GamePlayers.__init__(self)
        GameBoard.__init__(self)
        
        # Start up pygame, and initialize display
        pygame.init()
        self.screen = pygame.display.set_mode((1400, 800))
        pygame.display.set_caption("Royal Game of Ur")
        pygame.display.set_icon(pygame.image.load(lib._ROSETTE_IMAGE_PATH))
        self.screen.fill((0, 0, 0))

        # Apply game configuration
        self.cfg = GameConfiguration(args)

        if self.cfg.verbose >= 4:
            pygame.time.set_timer(self.gameLogicAlarm, 200) # Slow the timer to 200ms (to support all the terminal prints)
        else:
            pygame.time.set_timer(self.gameLogicAlarm, _GAME_TIMER) # 80ms, average as around 60ms


        # Draw Board
        self.drawGameBoard()

        # Determine starting player and assign the player turn (pTurn)
        self.pTurn = self.determineStartingPlayer()

    # --------------------------------------------------------------------------
    # Function: execute
    #
    # This function runs the simulation and holds it in place until the user 
    # stops it. 
    #---------------------------------------------------------------------------
    def execute(self):
        running = True
        simStarted = True 
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    # Flag to quite the game. 
                    running = False
                if event.type == self.timerAlarm:
                    # Updates the timer at one second. 
                    self.cfg.updateTime()
                if event.type == self.gameLogicAlarm:
                    startTime = time.time()
                    reroll = False

                    # Prevent execution of the game if the number of games is reached. 
                    if (self.cfg.currentGame <= self.cfg.NumberOfGames) and (simStarted == True):
                        rollValue = self.roll()
                        self.updateRollValueCnt(self.pTurn, rollValue)

                        reroll = self.determineMove(rollValue)
                        if reroll != True:
                            self.pTurn ^= 1
                        else:
                            self.logger.debug("REROLL!")

                        # Determine if the game is complete. 
                        if (self.Players[lib._PLAYER_1_IDX].score == 7) or (self.Players[lib._PLAYER_2_IDX].score == 7):
                            self.logger.debug("Game Over! Final Score: Player 1 ({}) | Player 2 ({})".format(self.Players[lib._PLAYER_1_IDX].score, self.Players[lib._PLAYER_2_IDX].score))
                            if self.Players[lib._PLAYER_1_IDX].score == 7:
                                # Player 1 won
                                self.incrementWin(lib._PLAYER_1_IDX)
                            else:
                                self.incrementWin(lib._PLAYER_2_IDX)

                            # Log data to records
                            self.cfg.updateGameRecord(self.Players)

                            # Reset game pieces and stats for each player
                            self.Players[lib._PLAYER_1_IDX].newGameReset()
                            self.Players[lib._PLAYER_2_IDX].newGameReset()

                            # Reset the board occupancy status
                            self.resetGameBoard()

                            # Start new game
                            if self.cfg.currentGame != self.cfg.NumberOfGames:
                                self.cfg.newGame()
                            else:
                                simStarted = False
           
                        # Update stats
                        self.screen.fill((0,0,0))
                        self.Players[lib._PLAYER_1_IDX].Pieces.update()
                        self.Players[lib._PLAYER_2_IDX].Pieces.update()
                    
                        # Draw / render
                        self.updateGameBoard(self.screen)

                        # Write square numbers in the upper corner, only if debug is active
                        if self.cfg.verbose >= 4:
                            for i in self.boardSquaresSpriteGroup.sprites():
                                self.textOnDisplay("{} ({})".format(i.id, i.uid), _BOARD_DEBUG_INFO, (255,255,255), i.rect[0]+10, i.rect[1]+10)

                        # Draw pieces
                        self.Players[lib._PLAYER_1_IDX].Pieces.draw(self.screen)
                        self.Players[lib._PLAYER_2_IDX].Pieces.draw(self.screen)

                        deltaTime  = time.time() - startTime
                        if deltaTime >= _GAME_TIMER:
                            self.logger.warn("{:.4f}".format(time.time() - startTime))

                        
                    else:
                        self.screen.fill((0,0,0))
                        self.textOnDisplay("Simulation Complete!", _END_SIM_TEXT, (255,255,255), 100, 100)

            

            # Redraw screen 
            pygame.display.flip()
            self.Clock.tick(self.FPS)

# ===[ EOF ]====================================================================
