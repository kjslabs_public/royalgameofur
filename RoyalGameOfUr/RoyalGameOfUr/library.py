#!/usr/bin/env python
# ==============================================================================
# ECE 802-602
# Royal Game of Ur - library.py
# Authors: Kevin Smith (smit2958@msu.edu)
#
# This file contains the class for any prompts that are displayed in the 
# terminal window.
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import os

# ===[ CONSTANTS ]==============================================================
# Images 
_ROSETTE_IMAGE_PATH = os.path.abspath('./img/rosette.png')
_GAMEBOARD_SQUARE_IMAGE_PATH = os.path.abspath('./img/gameBoardSquare.png')
_GAMEBOARD_SQUARE_BLANK_IMAGE_PATH = os.path.abspath('./img/gameBoardSquareBlank.png')
_GAMEPIECE_BLUE_IMAGE_PATH = os.path.abspath('./img/bluetoken.png')
_GAMEPIECE_ORANGE_IMAGE_PATH = os.path.abspath('./img/orangetoken.png')

# Fonts
_FONT_HACK_REGULAR = os.path.abspath('./font/Hack-Regular.ttf')

# CONSTANTS
_PLAYER_1_IDX = 0
_PLAYER_2_IDX = 1

# ===[ EOF ]====================================================================
