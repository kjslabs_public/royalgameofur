#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - RoyalGameOfUr.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This is the main file for this project that will launch the game when started. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import os
import sys
import traceback
import atexit

# Local inputs
import commandOptions as cmd
import gameCore as core

# ===[ MAIN ]===================================================================
# ------------------------------------------------------------------------------
# Function: main
#
# Main process of this script. Creates the game board and runs the simulation.
#-------------------------------------------------------------------------------
if __name__ == '__main__':
    os.system("mode con cols=100 lines=50")
    try:
        # Parse arguments
        args = cmd.processArg()

        # Create an instance of the game.
        game = core.RoyalGameOfUr(args)

        # This runs until the user quits
        game.execute()

    except KeyboardInterrupt:
        sys.exit()
    except Exception:
        traceback.print_exc(file=sys.stdout)
        sys.exit()

# ===[ EOF ]====================================================================
