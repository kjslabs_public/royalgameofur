#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - gamePiece.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This file contains the class for the game dice and to determine the player 
# that goes first. 
# ==============================================================================

# ===[ IMPORTS ]================================================================
import pygame
import library as lib

# ===[ DICTIONARIES ]==========================================================
_PLAYER_PIECES = { 0: lib._GAMEPIECE_BLUE_IMAGE_PATH,
                   1: lib._GAMEPIECE_ORANGE_IMAGE_PATH }

_PIECE_STATUS = {'Off_Board': 0,
                 'In_Play': 1,
                 'Scored': 2}

_GOAL_LOCATION = { 0: (100,100),
                   1: (100,100) }

# ===[ CLASSES ]===============================================================
# ------------------------------------------------------------------------------
# Class: Piece
#
# Core for each player piece and its properites.  
#-------------------------------------------------------------------------------
class Piece(pygame.sprite.Sprite):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self, player, pos_x, pos_y, id):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(pygame.image.load(_PLAYER_PIECES[player]), (50, 50))
        self.rect = self.image.get_rect()
        self.rect.topleft = (pos_x, pos_y)
        self.home = (pos_x, pos_y)
        self.pieceId = id
        self.squareId = 0
        self.squareUid = 0
        self.status = _PIECE_STATUS['Off_Board']

    # --------------------------------------------------------------------------
    # Function: move
    #
    # Move piece to the desired location. 
    #---------------------------------------------------------------------------
    def move(self, pos_x, pos_y):
        self.rect.topleft = (pos_x, pos_y)

    # --------------------------------------------------------------------------
    # Function: sendHome
    #
    # Return the piece back home and set status to off board. 
    #---------------------------------------------------------------------------
    def sendHome(self):
        self.rect.topleft = self.home
        self.status = _PIECE_STATUS['Off_Board']
        self.squareId = 0
        self.squareUid = 0

    # --------------------------------------------------------------------------
    # Function: setSquare
    #
    # Recoreds the square information the piece is located in. 
    #---------------------------------------------------------------------------
    def setSquare(self, uid, id, pos):
        self.rect.center = pos
        self.squareUid = uid
        self.squareId = id

# ===[ EOF ]====================================================================
