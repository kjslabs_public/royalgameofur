#!/usr/bin/env python
# ==============================================================================
# ECE 802-602
# Royal Game of Ur - gamePrompts.py
# Authors: Kevin Smith (smit2958@msu.edu)
#
# This file contains the class for any prompts that are displayed in the 
# terminal window.
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
from os import system, name

# ===[ CLASSES ]================================================================
# ------------------------------------------------------------------------------
# Class: GamePrompts
#
# Defines methods for clearing the terminal window and displaying common system
# messages to the user. 
#-------------------------------------------------------------------------------
class GamePrompts(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self):
        self.version = "1.0.0"

    #---------------------------------------------------------------------------
    # Function: __clearscreen
    #
    # Clears the terminal window based on the OS running the script. 
    #---------------------------------------------------------------------------
    def __clearscreen(self):
        if name == 'nt': # Windows
            system('cls')
        else: # MAC and Linux
            system('clear')

    #---------------------------------------------------------------------------
    # Function: displayWelcomeMessage
    #
    # Clears the terminal window based on the OS running the script. 
    #---------------------------------------------------------------------------
    def displayWelcomeMessage(self):
        self.__clearscreen()
        print("""
   ______                  _   _____                               __   _   _      
   | ___ \                | | |  __ \                             / _| | | | |     
   | |_/ /___  _   _  __ _| | | |  \/ __ _ _ __ ___   ___    ___ | |_  | | | |_ __ 
   |    // _ \| | | |/ _` | | | | __ / _` | '_ ` _ \ / _ \  / _ \|  _| | | | | '__|
   | |\ \ (_) | |_| | (_| | | | |_\ \ (_| | | | | | |  __/ | (_) | |   | |_| | |   
   \_| \_\___/ \__, |\__,_|_|  \____/\__,_|_| |_| |_|\___|  \___/|_|    \___/|_|   
     ---------- __/ | --------------------------------------------------------     
               |___/    --- A Strategy Simulator ---                           
""")
        print("    ")


# ===[ EOF ]====================================================================
