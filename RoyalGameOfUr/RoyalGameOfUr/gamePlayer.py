#!/usr/bin/env python
# ==============================================================================
# ECE 802-602
# Royal Game of Ur - gamePrompts.py
# Authors: Kevin Smith (smit2958@msu.edu)
#
# This file contains the class for any prompts that are displayed in the 
# terminal window.
# ==============================================================================

# ===[ IMPORTS ]================================================================
# System inputs
import pygame
import logging

# 3rd Party Imports
import colorlog.escape_codes as ec

# Local imports 
from gamePiece import Piece, _PIECE_STATUS


# ===[ DICTIONARIES ]==========================================================
_PLAYER_PIECE_STARTING_Y_POINT = {0: 160,
                                  1: 310}

                 #Index -4 -> 4, 0 (index 4) is current location
_AGGRESSIVE = [ [ 0,  0,  0,  0,  0,  0,  0,  0,  0],
                [ 1,  2,  3,  2,  0,  4,  1,  3,  1], # Roll value 1 to 4
                [ 1,  2,  3,  2,  0, -1,  4,  1,  3],
                [ 1,  2,  2,  2,  0, -2, -1,  4,  1],
                [ 1,  2,  3,  2,  0, -1, -2, -1,  4] ]

_AGGRESSIVE_NEWLOC = [ 1, 1, -3, -1,  0,  3,  4,  3,  2]                        


             # -4  -3 -2  -1   0   1   2   3   4
_PASSIVE = [ [ 0,  0,  0,  0,  0,  0,  0,  0,  0],
             [ 2,  3,  4,  3,  0,  5,  2,  3,  2], # Roll value 1 to 4
             [ 2,  3,  4,  3,  0, -5,  5,  2,  3],
             [ 2,  3,  4,  3,  0, -6, -5,  5,  2],
             [ 2,  3,  4,  3,  0, -5, -6, -5,  5] ]
_PASSIVE_NEWLOC = [ -1, -3, -4, -3,  0,  1,  1,  1,  1]

_MOVE_WEIGHT_MATRIX = [_AGGRESSIVE, _PASSIVE]
_MOVE_WEIGHT_MATRIX_NEWLOC = [_AGGRESSIVE_NEWLOC, _PASSIVE_NEWLOC]


_MOVE_WEIGHT = {'NoMove': 0,
                'OnBoarding': 2,
                'GenMove': 1,
                'Rosette': 30,
                'OnBoardMove': 8,
                'Capture': 4,
                'Score': 8}


# ===[ CLASSES ]================================================================
# ------------------------------------------------------------------------------
# Class: GamePlayer
#
# Class containing information about the player and their pieices  
#-------------------------------------------------------------------------------
class GamePlayer(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self, player):
        self.logger = logging.getLogger('')
        self.playerId = player
        self.wins = 0
        self.losses = 0
        self.moves = 0
        self.score = 0
        self.rollValCount = [0,0,0,0,0]
        self.Pieces = pygame.sprite.Group()
        for i in range(0, 7):
            self.Pieces.add(Piece(self.playerId, (940 + (i*60)), _PLAYER_PIECE_STARTING_Y_POINT[self.playerId], i))

    # --------------------------------------------------------------------------
    # Function: newGameReset
    #
    # Clears key variables back to start of game values. 
    #---------------------------------------------------------------------------
    def newGameReset(self):
        self.moves = 0
        self.score = 0
        self.rollValCount = [0,0,0,0,0]
        for piece in self.Pieces.sprites():
            piece.sendHome()

# ------------------------------------------------------------------------------
# Class: GamePlayers
#
# Class containing methods for how the players move and their decision making. 
#-------------------------------------------------------------------------------
class GamePlayers(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self):
        self.pTurn = 0
        self.Players = [GamePlayer(0), GamePlayer(1)]

    # --------------------------------------------------------------------------
    # Function: score
    #
    # Increment score for the player 
    #---------------------------------------------------------------------------
    def score(self, player):
        self.Players[player].score += 1

    # --------------------------------------------------------------------------
    # Function: incrementMove
    #
    # Increment move count for the player 
    #---------------------------------------------------------------------------
    def incrementMove(self, player):
        self.Players[player].moves += 1

    # --------------------------------------------------------------------------
    # Function: incrementWin
    #
    # Increment win count for the player and the loss count for the other. 
    #---------------------------------------------------------------------------
    def incrementWin(self, player):
        self.Players[player].wins += 1
        self.Players[player^1].losses += 1

    # --------------------------------------------------------------------------
    # Function: updateRollValueCnt
    #
    # Updates the roll value counting for each player 
    #---------------------------------------------------------------------------
    def updateRollValueCnt(self, player, value):
        self.Players[player].rollValCount[value] += 1

    # --------------------------------------------------------------------------
    # Function: removePieceFromBoard
    #
    # Look up the piece that is trying to be removed from the board. 
    #---------------------------------------------------------------------------
    def removePieceFromBoard(self, uid):
        for index, piece in enumerate(self.Players[self.pTurn^1].Pieces.sprites()):
            if piece.squareUid == uid:
                piece.sendHome()
                break

    # --------------------------------------------------------------------------
    # Function: determineMove
    #
    # Function determines if there is a move to be made. It looks at each piece
    # the player has to determine based on the roll value the options it has 
    # and the max value is selected for the move.  
    #---------------------------------------------------------------------------
    def determineMove(self, roll):
        self.logger.debug("Player {} rolled a {}".format(self.pTurn+1, roll))
        reroll = False
        
        if roll > 0:
            pieces = self.Players[self.pTurn].Pieces.sprites()
            availableMoves = [0] * len(pieces)
            availableUIDs = [-255] * len(pieces)
            for index, piece in enumerate(pieces):
                if piece.status != _PIECE_STATUS['Scored']:
                    availableUIDs[index], availableMoves[index] = self.checkMove(piece, roll)
                else:
                    availableMoves[index] = 0
            bestMoveWeight = max(availableMoves)
            bestMoveIndex = availableMoves.index(bestMoveWeight)
            if availableMoves[bestMoveIndex] > 0:

                # If there are multiple pieces with the same value, select the farthest ahead
                bestMoveIndexList = [index for index, moveWeight in enumerate(availableMoves) if moveWeight == bestMoveWeight]
                if len(bestMoveIndexList) > 1:
                    furthestPieceIdx = bestMoveIndexList[0]
                    for index in bestMoveIndexList:
                        if pieces[index].squareId > pieces[furthestPieceIdx].squareId:
                            furthestPieceIdx = index
                    bestMoveIndex = furthestPieceIdx

                # Move piece
                reroll = self.movePiece(pieces[bestMoveIndex], availableUIDs[bestMoveIndex])
                moveMade = True
                self.incrementMove(self.pTurn)
                self.logDebug("HomeSqId: {}, NewSqId: {}".format(piece.squareId, availableUIDs[bestMoveIndex]))
            else:
                moveMade = False
                self.logInfo("No move was able to be made")

        return reroll

    # --------------------------------------------------------------------------
    # Function: buildAnalysisLists
    #
    # Build two lists the contains squares to consider and the distance from
    # the piece.   
    #---------------------------------------------------------------------------
    def buildAnalysisLists(self, squares, location):
        # Build list of squares to analyze later
        consideredSquares = []
        consideredDistance = []
            
        # Gather squares behind new position
        for i in range (-4, 0):
            if (location >= 5) and (location <= 12):
                checkSq = location + i
                for item in squares:
                    if (checkSq > 0) and (checkSq <= 14):
                        if (item.id == checkSq) and ((self.pTurn^1) in item.player):
                            consideredSquares.append(item)
                            consideredDistance.append(i)
        # Gather square at and in front of new position
        for i in range (0,5):
            checkSq = location + i
            for item in squares:
                if (checkSq > 0) and (checkSq <= 15):
                    if (item.id == checkSq) and ((self.pTurn) in item.player):
                        consideredSquares.append(item)
                        consideredDistance.append(i)
    
        return consideredSquares, consideredDistance

    # --------------------------------------------------------------------------
    # Function: checkMove
    #
    # Check the value of the move.  
    #---------------------------------------------------------------------------
    def checkMove(self, piece, roll):
        moveWeight = _MOVE_WEIGHT['NoMove'] # Default value 
        uid = -255

        # Make sure the piece location + roll is valid. 
        destSquare = piece.squareId + roll

        if (destSquare <= 15): # Zero value roll is accounted for before this check so no need to do anything. 
            # Get the UID of the square
            uid = self.getSquareUID(self.pTurn, destSquare)
            squares = self.boardSquaresSpriteGroup.sprites()

            # Get data from the squares at your starting point and at the destination to determine move. 
            currentSquareAnalysis, currentSquareDistance = self.buildAnalysisLists(squares, piece.squareId)
            destSquareAnalysis, destSquareDistance = self.buildAnalysisLists(squares, destSquare)


            if piece.status == _PIECE_STATUS['Off_Board']:
                # Since the piece is off board, we can just check if the square is occupied, no need to 
                # look at the analysis for pre and post move.
                newSquare = destSquareAnalysis[destSquareDistance.index(0)]
                occupied, _ = newSquare.getOccupation()
                if occupied == False:
                    if newSquare.rosette == True:
                        moveWeight += _MOVE_WEIGHT['Rosette']
                    else:
                        moveWeight += _MOVE_WEIGHT['OnBoarding']
            elif piece.status == _PIECE_STATUS['In_Play']:
                # Now the piece is in play, so we need to determine the 'enviroment' of the board to determine what actions to take. 
                newSquare = destSquareAnalysis[destSquareDistance.index(0)]
                occupied, occupiedByPlayer = newSquare.getOccupation()

                if (destSquare == 15):
                    moveWeight = _MOVE_WEIGHT['Score']
                elif (occupied == True) and ((occupiedByPlayer == self.pTurn) or (squares[uid].rosette == True)):
                    # No move since it is occupied by another of your pieces, or it is a rosette that is occupied by either player. 
                    moveWeight += _MOVE_WEIGHT['NoMove']
                elif (occupied == False) and (squares[uid].rosette == True):
                    # Free square is a rosette, so no need to really look at area around because it is safe to move there
                    moveWeight += _MOVE_WEIGHT['Rosette']
                else:
                    # Occupied by opposition and is not a rosette, determine enviroment before moving. 
                    tmpStrategy = self.determinePlayerStrategy()
                    genMovePossible = False

                    # Update strategy usage
                    self.cfg.records[(self.cfg.currentGame - 1)].playerStats[self.pTurn].incrementStratCount(tmpStrategy)

                    # Check the surroundings before moving, determine if something needs to be done. 
                    for index, square in enumerate(currentSquareAnalysis):
                        if (square.occupied == True) and (square.occupiedByPlayer != self.pTurn):
                            # Occupied by the opposition, need to consider move based on strategy.
                            try:
                                if (square.rosette == True) and (currentSquareDistance[index] == 0) and (square.uid == 10):
                                    moveWeight = -5 # Set this to minus 5. Worst case this will be marked with 1 as a general move.
                                else:
                                    moveWeight += _MOVE_WEIGHT_MATRIX[tmpStrategy][roll][(currentSquareDistance[index] + 4)]
                            except:
                                raise Exception("Bad Index in current square analysis")
                        else:
                            # Occupied by yourself, or not occupied, thus nothing to worry about. 
                            continue
                          
                    # Check where you are going to make sure you are not moving to a bad place. 
                    for index, square in enumerate(destSquareAnalysis):
                        if (square.occupied == True) and (square.occupiedByPlayer != self.pTurn):
                            # Occupied by the opposition, need to consider move based on strategy.
                            try:
                                moveWeight += _MOVE_WEIGHT_MATRIX_NEWLOC[tmpStrategy][(destSquareDistance[index] + 4)]
                            except:
                                raise Exception("Bad Index in destination square analysis")
                        elif (square.uid == newSquare.uid) and (occupied == False):
                            genMovePossible = True
                        else:
                            # Occupied by yourself, or not occupied, thus to worry about. 
                            continue

                    # Finally, if the weight is negative, but a move exists, it must be considered, but bringing in a new piece has more weight, so unless
                    # the player cannot bring a new piece on board, this move needs to be considered, even if it is not ideal. 
                    if (genMovePossible == True):
                        moveWeight = _MOVE_WEIGHT['GenMove']
            else:
                # Scored peices should not be here as they are filtered out from the caller of this function. Placing this 
                # comment as a reminder. 
                pass  
            
        return uid, moveWeight # UID will always be the same for each piece
        
       

    # --------------------------------------------------------------------------
    # Function: movePiece
    #
    # Moves the piece on the board. 
    #---------------------------------------------------------------------------
    def movePiece(self, piece, uid):
        reroll = False
        try:
            square = (self.boardSquaresSpriteGroup.sprites())[uid]

            # If square was occupied by the other player, restart their piece and return them to start.
            if square.occupied == True and square.occupiedByPlayer != self.pTurn:
                self.removePieceFromBoard(uid)
            square.setOccupied(self.pTurn) # Mark square as occupied by the current player
            if square.rosette == True:
                reroll = True
        except:
            raise Exception("Error, out of bounds")
        

        if piece.squareId != 0: # Update the vacanted square that it is now free
            try:
                (self.boardSquaresSpriteGroup.sprites())[piece.squareUid].clearOccupied()
            except:
                raise Exception("Error, out of bounds")

        if square.id == 15:
            offset = self.Players[self.pTurn].score * 10
            piece.status = _PIECE_STATUS['Scored']
            piece.setSquare(square.uid, square.id, ((square.rect.center[0]-offset), square.rect.center[1] ))
            self.score(self.pTurn) # Update score last because the offset is used by the current score.
        else:
            # Move piece
            piece.status = _PIECE_STATUS['In_Play']
            piece.setSquare(square.uid, square.id, square.rect.center)

        return reroll

    # --------------------------------------------------------------------------
    # Function: logInfo
    #
    # Color code terminal window for the player for info messages
    #---------------------------------------------------------------------------
    def logInfo(self, msg):
        if self.pTurn == 0:
            esc = ec['bold_blue']
        else:
            esc = ec['bold_red']
        self.logger.info(esc + msg)

    # --------------------------------------------------------------------------
    # Function: logDebug
    #
    # Color code terminal window for the player for debug messages. 
    #---------------------------------------------------------------------------
    def logDebug(self, msg):
        if self.pTurn == 0:
            esc = ec['bold_blue']
        else:
            esc = ec['bold_red']
        self.logger.debug(esc + msg)

# ===[ EOF ]====================================================================  
