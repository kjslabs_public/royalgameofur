#!/usr/bin/env python
# ==============================================================================
# ECE 802-602 - Non-cooperative Game Theory
# Royal Game of Ur - gameBoard.py
# Author(s): Kevin Smith (smit2958@msu.edu)
#
# This file contains the classes and items required to draw the game board
# ==============================================================================

# ===[ IMPORTS ]================================================================
import pygame
import library as lib

# ===[ DICTIONARIES & CONSTANTS ]===============================================
_GAME_BOARD_SETTINGS_DICT = { 'width': 100,
                              'height': 100,
                              'starting_pos_x': 100,
                              'starting_pos_y': 100 }

# Font size and type for text displayed on the board. 
_TITLE_FONT = { 'font': lib._FONT_HACK_REGULAR,
                'size': 36 }

_PLAYER_SCORE_FONT = { 'font': lib._FONT_HACK_REGULAR,
                       'size': 36 }

_PLAYER_STAT_TITLE_FONT = { 'font': lib._FONT_HACK_REGULAR,
                            'size': 30 }

_PLAYER_STAT_ENTRY_FONT = { 'font': lib._FONT_HACK_REGULAR,
                            'size': 24 }

_PLAYER_INFO_FONT = { 'font': lib._FONT_HACK_REGULAR,
                      'size': 12 }

_BOARD_DEBUG_INFO = { 'font': lib._FONT_HACK_REGULAR,
                      'size': 6 }

# Font colors
_TEXT_COLOR_WHITE = (255, 255, 255)
_TEXT_COLOR_GRAY = (135, 135, 135)
_TEXT_COLOR_BLUE = (62, 143, 255)
_TEXT_COLOR_ORANGE = (240, 110, 0)

# Dictionary for chosing the font color based on player 1 (0) or player 2 (1)
_PLAYER_SCORE_FONT_COLOR = {0: _TEXT_COLOR_BLUE,
                            1: _TEXT_COLOR_ORANGE}

# Game board ID list for each player. Square 15 is the 'goal' and is not on the board
_GAMEBOARD_ID_LIST = [ [4,3,2,1,0,15,14,13],
                        [5,6,7,8,9,10,11,12],
                        [4,3,2,1,0,15,14,13] ]

# ID order for each player 
_GAMEBOARD_PLAYER_UID_ORDER = [ [3,2,1,0,7,8,9,10,11,12,13,14,6,5,4], 
                                [18,17,16,15,7,8,9,10,11,12,13,14,21,20,19] ]
_GAMEBOARD_GOAL_ID = 15
_GAMEBOARD_ROSETTE_ID_LIST = [4, 8, 14]
_UID_ = 0


# ===[ FUNCTIONS ]==============================================================
# --------------------------------------------------------------------------
# Function: uidGen()
#
# Return a number and increment for a unique ID to a list of items.
#---------------------------------------------------------------------------
def uidGen():
    global _UID_
    returnValue = _UID_
    _UID_ += 1 # Increment for next item
    return returnValue

# ===[ CLASSES ]================================================================
# ------------------------------------------------------------------------------
# Class: GameSquare
#
# Sprint of each of the game squares that make up the board. This item contains
# information about seach square including the ID, unique ID, if it is occupied
# a rosette, etc. 
#-------------------------------------------------------------------------------
class GameSquare(pygame.sprite.Sprite):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self, id, player):
        pygame.sprite.Sprite.__init__(self)
        self.id = id
        self.uid = uidGen()

        # Shared square are squares with that can have conflict, thus can be 
        # accessed by either player
        if (self.id >= 5) and (self.id <= 12):
            self.shared = True
            self.player = [lib._PLAYER_1_IDX, lib._PLAYER_2_IDX]
        else:
            self.shared = False
            self.player = [player]
        
        # Rosettes are safe from attack and give re-rolls
        if self.id in _GAMEBOARD_ROSETTE_ID_LIST:
            self.rosette = True
        else:
            self.rosette = False

        # Simple marker if the square is occupied and by which player 
        self.occupied = False
        self.occupiedByPlayer = None

        # Image information to load for sprite and if it is a goal or not. 
        if self.id == _GAMEBOARD_GOAL_ID:
            self.image = pygame.image.load(lib._GAMEBOARD_SQUARE_BLANK_IMAGE_PATH)
            self.goal = True
        else:
            self.image = pygame.image.load(lib._GAMEBOARD_SQUARE_IMAGE_PATH)
            self.goal = False
        self.rect = self.image.get_rect()

    #---------------------------------------------------------------------------
    # Function: setOccupied
    #
    # Updates the properites of the square with the current player that is 
    # in it.  
    #---------------------------------------------------------------------------
    def setOccupied(self, player):
        if (self.id != _GAMEBOARD_GOAL_ID):
            self.occupied = True
            self.occupiedByPlayer = player
        else:
            self.occupied = False
            self.occupiedByPlayer = None

    #---------------------------------------------------------------------------
    # Function: clearOccupied
    #
    # Resets the occupation status of a square to default.   
    #---------------------------------------------------------------------------
    def clearOccupied(self):
        self.occupied = False
        self.occupiedByPlayer = None

    #---------------------------------------------------------------------------
    # Function: getOccupation
    #
    # Returns the occupation status and the player of the square.   
    #---------------------------------------------------------------------------
    def getOccupation(self):
        return self.occupied, self.occupiedByPlayer


# ------------------------------------------------------------------------------
# Class: Rosette
#
# Simple class for each rosette in the game board to load the image and place it
# in the center of the parent square. 
#-------------------------------------------------------------------------------
class Rosette(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(pygame.image.load(lib._ROSETTE_IMAGE_PATH), (80, 80))
        self.rect = self.image.get_rect()

# ------------------------------------------------------------------------------
# Class: GameBoard
#
# Class to construct and maintain the game board. 
#-------------------------------------------------------------------------------
class GameBoard(object):
    # --------------------------------------------------------------------------
    # Function: __init__
    #
    # Consturctor function for the class.
    #---------------------------------------------------------------------------
    def __init__(self):
        self.logger.debug('Creating first instance of the Game Board')
        self.boardSquaresSpriteGroup = pygame.sprite.Group()
        self.rosetteSpriteGroup = pygame.sprite.Group()

    # --------------------------------------------------------------------------
    # Function: drawGameBoard
    #
    # Draws the game board in to the scene. 
    #---------------------------------------------------------------------------
    def drawGameBoard(self):
        
        # Draw board borders
        self.__drawBoardEdges()

        # Place rosettes in board
        self.__drawRosettes()

        # Board Title
        self.__drawTitle()

        # Player Score Information
        self.updatePlayerScore()
        
        # Draw Player Information
        self.updatePlayerStat()

    # --------------------------------------------------------------------------
    # Function: __drawBoardEdges
    #
    # Internal function that creates each of the sqaures to create the board
    #---------------------------------------------------------------------------    
    def __drawBoardEdges(self):
        for row in range(0,3):
            # Assign player 
            if row == 0:
                player = 0
            elif row == 2:
                player = 1
            else:
                player = None

            for column in range(0,8):
                if ((row == 0) or (row == 2)) and (column == 4):
                    continue
                else:
                    item = GameSquare(_GAMEBOARD_ID_LIST[row][column], player)
                    item.rect.topleft = ((_GAME_BOARD_SETTINGS_DICT['starting_pos_x'] + (column * _GAME_BOARD_SETTINGS_DICT['width'])),
                                         (_GAME_BOARD_SETTINGS_DICT['starting_pos_y'] + (row * _GAME_BOARD_SETTINGS_DICT['height'])) )
                    self.boardSquaresSpriteGroup.add(item)

    # --------------------------------------------------------------------------
    # Function: resetGameBoard
    #
    # Clears the occupation for each square on the board. 
    #---------------------------------------------------------------------------   
    def resetGameBoard(self):
        for square in self.boardSquaresSpriteGroup.sprites():
            square.clearOccupied()
                    
    # --------------------------------------------------------------------------
    # Function: __drawBoardEdges
    #
    # Get the requested sqaures unique ID from the given player ID. Note that 
    # the player square ID starts at 1, so -1 is used to remove that offset.
    #---------------------------------------------------------------------------   
    def getSquareUID(self, player, id):
        return (self.boardSquaresSpriteGroup.sprites())[_GAMEBOARD_PLAYER_UID_ORDER[player][id-1]].uid
                    
    # --------------------------------------------------------------------------
    # Function: __drawRosettes
    #
    # Internal function to create a sprite group of the rosettes that are placed
    # on the game baord. 
    #---------------------------------------------------------------------------  
    def __drawRosettes(self):
        for square in self.boardSquaresSpriteGroup.sprites():
            if square.rosette:
                rosette = Rosette()
                rosette.rect.center = square.rect.center
                self.rosetteSpriteGroup.add(rosette)

    # --------------------------------------------------------------------------
    # Function: __drawTitle
    #
    # Draws the title on teh game board screen.  
    #--------------------------------------------------------------------------- 
    def __drawTitle(self):
        self.textOnDisplay('-- The Royal Game of Ur --', _TITLE_FONT, (80,80,80), 210, 30)

    # --------------------------------------------------------------------------
    # Function: updateGameBoard
    #
    # Updates the screen with the new score, stats, and status of the game.  
    #--------------------------------------------------------------------------- 
    def updateGameBoard(self, screen):
        self.__drawTitle()
        self.updatePlayerScore()
        self.updatePlayerStat()
        self.updateSimStatus()
        self.boardSquaresSpriteGroup.draw(screen)
        self.rosetteSpriteGroup.draw(screen)

    # --------------------------------------------------------------------------
    # Function: updateSimStatus
    #
    # Updates simulation status section (called by updateGameBoard)  
    #--------------------------------------------------------------------------- 
    def updateSimStatus(self):
        label_posx = 940
        label_posy = 420
        self.textOnDisplay('Simulation Status', _PLAYER_STAT_TITLE_FONT, _TEXT_COLOR_WHITE, label_posx, label_posy)
        self.textOnDisplay('Number of Games:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (label_posx + 20), (label_posy + 50))
        self.textOnDisplay('{}'.format(self.cfg.NumberOfGames), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, 1185, (label_posy + 50))

        self.textOnDisplay('Current Game Number:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (label_posx + 20), (label_posy + 80))
        self.textOnDisplay('{}'.format(self.cfg.currentGame), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, 1240, (label_posy + 80))

        self.textOnDisplay('Time:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (label_posx + 20), (label_posy + 110))
        self.textOnDisplay('{}'.format(self.cfg.getTime()), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, 1030, (label_posy + 110))

        self.textOnDisplay('Min Time:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (label_posx + 20), (label_posy + 140))
        self.textOnDisplay('{}'.format(self.cfg.getTimeMin()), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, 1085, (label_posy + 140))

        self.textOnDisplay('Max Time:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (label_posx + 20), (label_posy + 170))
        self.textOnDisplay('{}'.format(self.cfg.getTimeMax()), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, 1085, (label_posy + 170))

        self.textOnDisplay('Average Time:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (label_posx + 20), (label_posy + 200))
        self.textOnDisplay('{:.2f}'.format(self.cfg.getTimeAverage()), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, 1140, (label_posy + 200))
        
    # --------------------------------------------------------------------------
    # Function: updatePlayerStat
    #
    # Updates the player stats section of the screen (called by updateGameBoard)  
    #--------------------------------------------------------------------------- 
    def updatePlayerStat(self):
        self.drawPlayerStat(1, 125, 420)
        self.drawPlayerStat(2, 625, 420)

    # --------------------------------------------------------------------------
    # Function: updatePlayerScore
    #
    # Updates the player score section of the screen (called by updateGameBoard)  
    #--------------------------------------------------------------------------- 
    def updatePlayerScore(self):
        self.drawPlayerScore(1, 940, 100)
        self.drawPlayerScore(2, 940, 250)
        
    # --------------------------------------------------------------------------
    # Function: drawPlayerScore
    #
    # Renders the image to be displayed on the screen for the player score.  
    #--------------------------------------------------------------------------- 
    def drawPlayerScore(self, player, pos_x, pos_y):
        self.textOnDisplay('Player {} Score:'.format(player), _PLAYER_SCORE_FONT, _TEXT_COLOR_WHITE, pos_x, pos_y)
        self.textOnDisplay('{}'.format(self.Players[player-1].score), _PLAYER_SCORE_FONT, _PLAYER_SCORE_FONT_COLOR[player-1], (pos_x + 340), pos_y)

    # --------------------------------------------------------------------------
    # Function: drawPlayerStat
    #
    # Renders the image to be displayed on the screen for the player stats.  
    #--------------------------------------------------------------------------- 
    def drawPlayerStat(self, player, pos_x, pos_y):
        self.textOnDisplay('Player {} Stats'.format(player), _PLAYER_STAT_TITLE_FONT, _TEXT_COLOR_WHITE, pos_x, pos_y)
        self.textOnDisplay('Wins:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (pos_x + 20), (pos_y + 50))
        self.textOnDisplay('{}'.format(self.Players[player-1].wins), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, (pos_x + 115), (pos_y + 50))
        self.textOnDisplay('Loses:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (pos_x + 20), (pos_y + 80))
        self.textOnDisplay('{}'.format(self.Players[player-1].losses), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, (pos_x + 115), (pos_y + 80))
        self.textOnDisplay('Moves:', _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_WHITE, (pos_x + 20), (pos_y + 110))
        self.textOnDisplay('{}'.format(self.Players[player-1].moves), _PLAYER_STAT_ENTRY_FONT, _TEXT_COLOR_GRAY, (pos_x + 115), (pos_y + 110))

    # --------------------------------------------------------------------------
    # Function: textOnDisplay
    #
    # Helper function to load the font, and render the text on the screen. 
    #---------------------------------------------------------------------------        
    def textOnDisplay(self, msg, font, color, pos_x, pos_y):
        loadedFont = pygame.font.Font(font['font'], font['size'])
        msg = loadedFont.render(msg, True, color, (0,0,0))
        msgRect = msg.get_rect()
        msgRect.topleft = (pos_x, pos_y)

        # Screen is from the Royal Game of Ur Class
        self.screen.blit(msg, msgRect)

# ===[ EOF ]====================================================================
